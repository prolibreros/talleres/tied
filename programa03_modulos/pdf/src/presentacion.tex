\chapter{Presentación}

\pagestyle{fancy}
\fancyhead{}
\fancyhead[CO]{\footnotesize\textsc{Presentación}\normalsize}
\fancyhead[CE]{\footnotesize\textsc{\myauthor}\normalsize}

\vskip 3em

En 1492 Johannes Trithemius llevó a cabo una crítica a las nuevas
técnicas de reproducción de textos. En su
\href{http://gen.lib.rus.ec/book/index.php?md5=60B7C01C3FE0D9FCDA4629736BB3C382}{\emph{Elogio
de los amanuenses}} desestima a la imprenta por su intento de llevar
libros baratos y de baja calidad a las masas. Según su postura, el
cuidado en la copia manual de textos hechos por los monjes es
incomparable al trabajo realizado por las máquinas.

Si bien en la actualidad la crítica de Trithemius nos parece
desproporcionada ---tratándose incluso de un síndrome que se caracteriza
por el rechazo al cambio a partir de argumentos que pretenden proteger
una zona de confort y, por supuesto, los mecanismos de poder
vigentes---, es cierto que la calidad y la diversidad de soportes que
ahora llamamos libros han disminuido a la par que su cantidad, tanto de
número de obras como de ejemplares, ha aumentado de manera estrepitosa.
Lo que hoy en día conocemos como «libro» es el producto de una paulatina
homogeneización en las técnicas y las metodologías empleadas para
elaborarlos. Es decir, desde el advenimiento de la imprenta los modos
contemporáneos de producción de publicaciones han apostado por una serie
de estandarizaciones en pos del aumento de la producción.

Esto deja la calidad de los objetos producidos en un segundo plano o
constreñida a las posibilidades tecnológicas reinantes. De una u otra
forma se condiciona la diversidad de soportes a la aparente necesidad de
objetos innovativos para hacer más eficiente la acumulación de capital.
Quizá Trithemius temía ---aunque forzando esta interpretación a la luz
de la crisis editorial actual--- la pérdida de la diversidad de las
técnicas y las metodologías de producción editorial, en particular
aquellas cuya finalidad no era económica, sino la conservación de una
tradición.

El discurso ilustrado dotó de una justificación humanística a estas
nuevas técnicas de reproducción de textos a la par que obtuvo beneficios
por la amplificación de su discurso a través de la producción de una
mayor cantidad de sus textos impresos. Lo que se perdió en calidad en
cuanto al objeto, se ganó en la cantidad de sujetos que ahora tienen
acceso a su contenido. El aumento de la producción se defendió como
\emph{el} mecanismo para la propagación de la lectura y, con ello, del
conocimiento y de la cultura.

Así como Thrithemius le tocó vivir en una etapa de transición ---donde
hubo un súbito aumento en las posibilidades futuras de su profesión pero
con el costo de una crisis de sus modos vigentes de producción---, hoy
ya es prácticamente innegable que la edición está pasando por una nueva
etapa de transformación. Este monje alemán vivió la mudanza de los
procesos manuales a los maquinales. Desde mediados de los ochenta
nosotros estamos advirtiendo la migración de tecnologías análogas a las
digitales.

El papel y la tinta, aunque presente todavía de manera predominante en
la economía de la industria editorial, ya no es el soporte primigenio
por el cual se organizan los procesos de producción de publicaciones.
Son la corriente eléctrica y los bits los que establecen la disposición
los medios para la producción de publicaciones impresas o digitales. El
papel y la tinta ya no son elementos regulativos en la publicación, sino
una consecuencia de otro paradigma que está siendo alimentado por las
nuevas tecnologías de la información y la comunicación ---tal vez para
satisfacer una demanda por el papel que, según, cada nueva generación
empieza a solicitar en menor medida---.

Estas transformaciones están provocando una tensión en el sector
editorial entre la preservación y la innovación. La polarización no se
hace esperar. Por un lado, personas semejantes a Thrithemius defienden
la pérdida de la calidad editorial en mano de las nuevas técnicas y
metodologías de producción editorial y, así, la búsqueda de estrategias
para conservar los mecanismos de poder vigentes. Por ejemplo, el
atropello de inspeccionar y calificar cualquier soporte a partir de las
cualidades de los impresos o, de manera más desproporcionada, el
desarrollo de tecnologías que permiten un control en la difusión
semejante al que se ejerce en la distribución de publicaciones impresas,
como es el caso del \textsc{drm} (\emph{Digital Rights Management}).

Por otro lado, y aunque con menos ímpetu que hace una década, hay
personas que cantan la muerte de los soportes impresos. Este polo de la
crisis apela a la emancipación de los antiguos aparatos de producción y
de reproducción de la cultura y el conocimiento, de manera muy similar
al llamado de la Ilustración que, a través de la imprenta, difundió sus
discursos de liberación. No más instituciones «monolíticas», como la
universidad o la editorial, o cadenas simbólicas, como las restricciones
del papel, sino más personas cara a cara produciendo cultura y
conocimiento «inmaterial». Menos determinaciones y menos intermediarios
para el acceso a la palabra: más \emph{individuos} y más apertura, como
los bazares.

Sin embargo, la situación económica y política actual no permite agitar
la bandera de la Ilustración sin antes tomar en cuenta un patente
peligro. Lo que en el siglo \textsc{xviii} fue un parteaguas para la
gestación de un nuevo orden social ---la burguesía y el proletariado---
hoy en día se presta más a la conservación de ese orden y no a su crisis
o su superación. El desarrollo tecnológico actual exige de constantes
innovaciones y la diversificación de soportes para mantener los índices
de producción y para sustentar una economía global que se basa en la
acumulación de capital.

En el caso de la industria editorial, el abrigo de las nuevas
tecnologías y el aumento en la producción de nuevos soportes no son a
favor de una liberación de la profesión de sus antiguas formas de
organización, sino un camino para salvaguardar la economía que esta
industria ha hecho posible. No se trata de la liberación de los
profesionistas dedicados a la edición o de los lectores, sino de la
preservación del monopolio de una industria a través de la
diversificación y tecnificación de su infraestructura.

En este sentido es entendible que en lugar de técnicas o métodos, el
enfoque pedagógico actual del sector editorial sea principalmente a
través del ofrecimiento de «soluciones» y de la mutación del objeto
textual en «contenido». Las soluciones en las que se vuelcan diversos
posgrados, diplomados o talleres por lo general tienen tras de sí una
currícula oculta que hace de los profesionistas los clientes
dependientes de estas nuevas tecnologías. Como se sabe, estos tipos de
propiedad intelectual están custodiadas por grandes consorcios
editoriales o empresas tecnológicas. Una pedagogía basada en programas
computacionales y en \emph{tips} para hacer más eficiente su uso es, a
lo sumo, el desarrollo de habilidades que subordinan al alumno a las
posibilidades ofrecidas por los desarrolladores de \emph{software} y la
industria editorial.

El traslado discursivo del texto al contenido no solo es la búsqueda de
un eufemismo \emph{ad hoc} a lo que está en boga en los medios de
comunicación. Este cambio en el discurso implica una pérdida de
especificidad del producto textual para así pretender colocar su
producción en la misma posición en la que se encuentra el desarrollo de
otro tipo de objetos culturales, como el \emph{software}, el audio o el
video. En esta homologación del libro a otro tipo de economías se pierde
el carácter distintivo de la profesión.

La currícula que se ofrece a continuación es una respuesta ante las
problemáticas expuestas aquí. Como podrá observarse, trata a los
procesos editoriales de una manera muy distintiva y ampliamente técnica.
Es decir, no pretende entender a la edición en su sentido habitual
---como una profesión, un arte o una tradición---. En esta currícula la
edición es fundamentalmente metodología y técnica sin volcarse de lleno
a dar soporte a la ola de «innovación» presente en el sin fin de eventos
y capacitaciones que se organizan en torno al libro. Para esta currícula
una innovación de los objetos producidos exige una reflexión sobre la
dependencia tecnológica imperante y una práctica que dé habilidades al
profesionista con el menor número de ataduras.

Esta currícula supone que un enfoque pedagógico sobre los métodos y los
modos de producción editorial ayudará al estudiante a mantener un
desarrollo progresivo de sus habilidades, independientemente de los
programas computacionales en boga. El costo a pagar por ello es
adentrarse más a lo que está supuesto en cada formato digital e interfaz
gráfica de trabajo. La curva de aprendizaje es, sin duda, mayor a las
que podrían encontrarse al momento de aprender a usar \emph{software}.
Sin embargo, tiene mayores posibilidades de no ser interrumpida por
nuevas «soluciones» o el aumento en la popularidad de programas de
cómputo o soportes.

Es decir, la necesidad de mayor tiempo de aprendizaje tiene como
consecuencia un ahorro en los recursos humanos necesarios para adaptarse
a cada nuevo parámetro de la industria. Los efectos más palpables de
ello es que el estudiante de esta currícula poco a poco será un
profesionista más especializado, ahorrándose así tiempo y dinero para su
preparación.

A sabiendas de que esto por sí solo no cuestiona el aumento de la
productividad bajo las políticas y economías actuales, esta currícula
espera contribuir al debate con otro tipo de estrategia. Una crítica
sobre una profesión y una industria se vuelve ineficaz si de manera
previa no existe un dominio sobre las técnicas que le dan cabida. Esta
currícula supone que en cierto grado la polarización en el sector
editorial se debe a un amplio desconocimiento de lo que está detrás de
las técnicas aplicadas y de lo que otras tecnologías podrían hacer
posible. Un mayor dominio sobre este terreno, tal como se pretende aquí,
podrá ayudar a generar puntos de encuentro y a matizar lo que ambas
posturas defienden. Por ese motivo, aunque el aumento en la
productividad es uno de los efectos de este programa, su énfasis en el
uso de ciertas tecnologías busca ofrecer al estudiante un panorama más
integral de lo que ahora está en juego cuando hablamos sobre edición de
publicaciones.

Si la corriente eléctrica y los bits ahora configuran los modos de
producción editorial, no hay duda de que es necesario abordarlos, sea
para su especialización o su transformación. El profesionista de la
edición no dejará de ser cliente si no adquiere los conocimientos y las
habilidades para convertirse en su propio arquitecto.
