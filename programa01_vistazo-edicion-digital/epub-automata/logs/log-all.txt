Pecas: 2018.10.15-3a2e791
 Ruby: 2.4.3-p205
 Host: x86_64-pc-linux-gnu

Software bajo licencia GPLv3+: <https://gnu.org/licenses/gpl.html>.
Documentación: <http://pecas.cliteratu.re/>.

------------------------------------------------------------------

# pc-pandog
$ ruby /home/nika-zhenya/Repositorios/pecas/epub/automata/../../base-files/pandog/pandog.rb -i /home/nika-zhenya/Repositorios/tied/archivos-madre/md/todo.md -o /home/nika-zhenya/Repositorios/tied/epub-automata/.todo.xhtml

# pc-creator
$ ruby /home/nika-zhenya/Repositorios/pecas/epub/automata/../creator/creator.rb -o epub -c /home/nika-zhenya/Repositorios/tied/archivos-madre/img/portada.jpg -i /home/nika-zhenya/Repositorios/tied/archivos-madre/img     --no-pre

# pc-divider
$ ruby /home/nika-zhenya/Repositorios/pecas/epub/automata/../divider/divider.rb -f /home/nika-zhenya/Repositorios/tied/epub-automata/.todo.xhtml -d epub/OPS/xhtml -s epub/OPS/css/styles.css -i 3 --section

# pc-recreator
$ ruby /home/nika-zhenya/Repositorios/pecas/epub/automata/../recreator/recreator.rb -d epub -y /home/nika-zhenya/Repositorios/tied/epub-automata/meta-data.yaml 

# pc-changer
$ ruby /home/nika-zhenya/Repositorios/pecas/epub/automata/../changer/changer.rb -e /home/nika-zhenya/Repositorios/tied/epub-automata/epub-c60acce0.epub --version 3.0.0

# pc-analytics
$ ruby /home/nika-zhenya/Repositorios/pecas/epub/automata/../../base-files/analytics/analytics.rb -f /home/nika-zhenya/Repositorios/tied/epub-automata/epub-c60acce0.epub --json 

# epubcheck 4.0.2
$ java -jar /home/nika-zhenya/Repositorios/pecas/epub/automata/../../src/alien/epubcheck/4-0-2/epubcheck.jar epub-c60acce0.epub -out log.xml -q

  <?xml version="1.0" encoding="UTF-8"?>
  <jhove xmlns="http://hul.harvard.edu/ois/xml/ns/jhove"
         date="2016-11-29"
         name="epubcheck"
         release="4.0.2">
     <date>2018-10-17T19:01:41-05:00</date>
     <repInfo uri="epub-c60acce0.epub">
        <created>2018-10-17T19:01:35Z</created>
        <lastModified>2018-10-17T19:01:36Z</lastModified>
        <format>application/epub+zip</format>
        <version>3.0.1</version>
        <status>Well-formed</status>
        <mimeType>application/epub+zip</mimeType>
        <properties>
           <property>
              <name>CharacterCount</name>
              <values arity="Scalar" type="Long">
                 <value>20086</value>
              </values>
           </property>
           <property>
              <name>Language</name>
              <values arity="Scalar" type="String">
                 <value>es</value>
              </values>
           </property>
           <property>
              <name>Info</name>
              <values arity="List" type="Property">
                 <property>
                    <name>Identifier</name>
                    <values arity="Scalar" type="String">
                       <value>1_0_0-c60acce00189404aba65a8717f33c452</value>
                    </values>
                 </property>
                 <property>
                    <name>CreationDate</name>
                    <values arity="Scalar" type="Date">
                       <value>2018-10-17T19:01:35Z</value>
                    </values>
                 </property>
                 <property>
                    <name>ModDate</name>
                    <values arity="Scalar" type="Date">
                       <value>2018-10-17T19:01:36Z</value>
                    </values>
                 </property>
                 <property>
                    <name>Title</name>
                    <values arity="Scalar" type="String">
                       <value>Taller Intensivo de Edición Digital</value>
                    </values>
                 </property>
                 <property>
                    <name>Creator</name>
                    <values arity="Array" type="String">
                       <value>Bayardo, Melisa</value>
                       <value>Santa Ana Anguiano, Ramiro</value>
                    </values>
                 </property>
              </values>
           </property>
           <property>
              <name>Fonts</name>
              <values arity="List" type="Property">
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Bitter Regular</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Bitter Italic</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Bitter Bold</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Bitter BoldItalic</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
              </values>
           </property>
           <property>
              <name>MediaTypes</name>
              <values arity="Array" type="String">
                 <value>application/x-dtbncx+xml</value>
                 <value>application/xhtml+xml</value>
                 <value>image/jpeg</value>
                 <value>text/css</value>
                 <value>application/vnd.ms-opentype</value>
              </values>
           </property>
        </properties>
     </repInfo>
  </jhove>

# epubcheck 3.0.1
$ java -jar /home/nika-zhenya/Repositorios/pecas/epub/automata/../../src/alien/epubcheck/3-0-1/epubcheck.jar epub-c60acce0_3-0-0.epub -out log.xml -q

  <?xml version="1.0" encoding="UTF-8"?>
  <jhove xmlns="http://hul.harvard.edu/ois/xml/ns/jhove" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" name="epubcheck" release="3.0.1" date="2013-05-10">
   <date>2018-10-17T19:01:43-05:00</date>
   <repInfo uri="epub-c60acce0_3-0-0.epub">
    <created>2018-10-17T19:01:35Z</created>
    <lastModified>2018-10-17T19:01:36Z</lastModified>
    <format>application/epub+zip</format>
    <version>3.0</version>
    <status>Well-formed</status>
    <mimeType>application/epub+zip</mimeType>
    <properties>
     <property><name>CharacterCount</name><values arity="Scalar" type="Long"><value>20086</value></values></property>
     <property><name>Language</name><values arity="Scalar" type="String"><value>es</value></values></property>
     <property><name>Info</name><values arity="List" type="Property">
      <property><name>Identifier</name><values arity="Scalar" type="String"><value>1_0_0-c60acce00189404aba65a8717f33c452</value></values></property>
      <property><name>CreationDate</name><values arity="Scalar" type="Date"><value>2018-10-17T19:01:35Z</value></values></property>
      <property><name>ModDate</name><values arity="Scalar" type="Date"><value>2018-10-17T19:01:36Z</value></values></property>
      <property><name>Title</name><values arity="Scalar" type="String"><value>Taller Intensivo de Edición Digital</value></values></property>
      <property><name>Creator</name><values arity="Array" type="String"><value>Bayardo, Melisa</value><value>Santa Ana Anguiano, Ramiro</value></values></property>
     </values></property>
     <property><name>Fonts</name><values arity="List" type="Property">
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Bitter Regular</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Bitter Italic</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Bitter Bold</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Bitter BoldItalic</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
     </values></property>
    </properties>
   </repInfo>
  </jhove>

# ace
$ ace -o logs/ace epub-c60acce0.epub

  info:    Processing epub-c60acce0.epub
    info:    Parsing EPUB
    info:    Analyzing accessibility metadata
    info:    Checking package...
    info:    - OPS/content.opf: No issues found
    info:    Checking documents...
    info:    - xhtml/003-portada.xhtml: No issues found
    info:    - xhtml/004-descripcion.xhtml: No issues found
    info:    - xhtml/005-informacion_general.xhtml: No issues found
    info:    - xhtml/007-costos.xhtml: No issues found
    info:    - xhtml/006-syllabus.xhtml: No issues found
    info:    Consolidating results...
    info:    Copying data
    info:    Saving JSON report
    info:    Saving HTML report
    info:    Done.
  

# kindlegen
$ kindlegen epub-c60acce0.epub

  *************************************************************
   Amazon kindlegen(Linux) V2.9 build 1028-0897292 
   A command line e-book compiler 
   Copyright Amazon.com and its Affiliates 2014 
  *************************************************************
  
  Info(prcgen):I1047: Added metadata dc:Title        "Taller Intensivo de Edición Digital"
  Info(prcgen):I1047: Added metadata dc:Creator      "Bayardo, Melisa"
  Info(prcgen):I1047: Added metadata dc:Creator      "Santa Ana Anguiano, Ramiro"
  Info(prcgen):I1047: Added metadata fixed-layout    "false"
  Info(prcgen):I1002: Parsing files  0000005
  Warning(htmlprocessor):W28001: CSS style specified in content is not supported by Kindle readers. Removing the CSS property: 'column-count' in file: /tmp/mobi-MpUgWW/OPS/css/styles.css
  Warning(htmlprocessor):W28001: CSS style specified in content is not supported by Kindle readers. Removing the CSS property: 'column-gap' in file: /tmp/mobi-MpUgWW/OPS/css/styles.css
  Warning(htmlprocessor):W28001: CSS style specified in content is not supported by Kindle readers. Removing the CSS property: 'column-rule' in file: /tmp/mobi-MpUgWW/OPS/css/styles.css
  Info(prcgen):I1016: Building enhanced PRC file
  Info(prcgen):I1015: Building PRC file
  Info(prcgen):I1006: Resolving hyperlinks
  Info(pagemap):I8000: No Page map found in the book
  Info(prcgen):I1045: Computing UNICODE ranges used in the book
  Info(prcgen):I1046: Found UNICODE range: Basic Latin [20..7E]
  Info(prcgen):I1046: Found UNICODE range: Latin-1 Supplement [A0..FF]
  Info(prcgen):I1046: Found UNICODE range: General Punctuation - Windows 1252 [2026..2026]
  Info(prcgen):I1017: Building PRC file, record count:   0000008
  Info(prcgen):I1039: Final stats - text compressed to (in % of original size):  41.77%
  Info(prcgen):I1040: The document identifier is: "Taller_Inten-dicion_Digital"
  Info(prcgen):I1041: The file format version is V6
  Info(prcgen):I1031: Saving PRC file
  Info(prcgen):I1032: PRC built successfully
  Info(prcgen):I1007: Resolving mediaidlinks
  Info(prcgen):I1011: Writing mediaidlinks
  Info(prcgen):I1009: Resolving guide items
  Info(prcgen):I1017: Building PRC file, record count:   0000011
  Info(prcgen):I1039: Final stats - text compressed to (in % of original size):  44.53%
  Info(prcgen):I1041: The file format version is V8
  Info(prcgen):I15000:  Approximate Standard Mobi Deliverable file size :   0000078KB
  Info(prcgen):I15001:  Approximate KF8 Deliverable file size :   0000217KB
  Info(prcgen):I1036: Mobi file built successfully
  
