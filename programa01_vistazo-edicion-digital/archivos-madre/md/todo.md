<section id="portadilla" epub:type="titlepage">

# La edición se hizo código {.centrado .titulo}

## Taller intensivo de edición digital {.centrado .subtitulo}

Melisa Bayardo \
Ramiro Santa Ana Anguiano {.centrado}

# Portada @ignore

</section>
<section epub:type="chapter" role="doc-chapter">

# Descripción

La edición es ya edición digital. ¿Usas una computadora para tus libros
impresos o digitales? ¿La imprenta a la que acudes solo te recibe formatos
digitales? El uso de computadoras y archivos digitales no es una primicia
en la edición. La discusión entre las publicaciones en papel y las
electrónicas ya no es una novedad.

Sin embargo, la tradición editorial ha ignorado que _no es un cambio de 
técnicas ni de dispositivos sino la pérdida de los fundamentos básicos 
de la edición_:

* La edición no solo es una profesión, un arte o una tradición, también 
  es un método y una pedagogía.
* El texto no solo es diseño y contenido, asimismo es estructura.
* Las publicaciones ya no solo se editan y diseñan, del mismo modo se
  programan.

## Antecedentes

El quehacer editorial se ha valido de un conjunto de saberes, tecnologías
y técnicas en pos de una mayor calidad en el menor tiempo posible. Desde
el nacimiento de la imprenta hasta antes del surgimiento de las 
tecnologías digitales es posible rastrear una línea evolutiva.

El objetivo era claro: publicar de la manera más eficiente y eficaz 
posible. Sin embargo, este ideal se regía por un simple supuesto: la obra
siempre se fijaba en un solo formato. 

La publicación era la obra hecha papel. La sinonimia era tal que no había
una distinción clara entre los conceptos de «obra», «libro», «soporte» y
«formato». La relación era tan estrecha que la definición de cada concepto
siempre implicaba las nociones de «diseño», «contenido», «papel» y
«página».

Sin conciencia clara, el surgimiento de las nuevas tecnologías de la 
información y la comunicación dinamitaron el supuesto que regía al quehacer
editorial. En tan solo una década la idea de obra se diversificó a su
concreción en diversos formatos y soportes. El papel y la página dejaron
de ser características definitorias del trabajo editorial. El diseño y el
cuidado del contenido comenzaron a subordinarse a las estructuras 
textuales que incluso hace posible prescindir de la escritura.

Como editores, ¿qué nos queda y dónde nos ubicamos cuando los elementos
clave de la edición son ya características secundarias o llanas metáforas?

## Proyección

La edición como método siempre ha sido parte de la tradición. La cuestión
estriba en que por lo general fue confundida por la experiencia, la 
intuición, la creatividad y la genialidad de cada persona dedicada a los 
procesos editoriales.

Cuando la producción de un objeto implica una serie de procesos; cuando
estos procesos llegan a ser tan monótonos y maquinales, en realidad de lo
que hablamos es de un método. Cuando este método se desconoce y de repente
se ha de aprender; cuando las habilidades editoriales se afinan en el
trabajo mismo, lo que se está realizando es un proceso pedagógico.

El objetivo de la edición se vuelve más complejo: publicar multiformato
de la manera más eficaz y eficiente posible. Esto acarrea un nuevo 
supuesto: la edición tiene que automatizarse. Pero para ello se requiere
una redefinición de la edición como método y pedagogía de producción.

Este taller intensivo es una introducción a esta nueva manera de pensar la
edición.

</section>
<section epub:type="chapter" role="doc-chapter">

# Información general

El taller se planea realizarse en diez sesiones intensivas con un total de
veinticinco horas. Las primeras cinco sesiones serían de dos horas y las 
restantes de tres. 

## Objetivos

El taller intensivo pretende dotar de los elementos mínimos necesarios para
una producción más eficiente de publicaciones. Aunque en la actualidad la
metodología propuesta responde de mejor manera a formatos digitales,
también es aplicable a medios impresos.

### General

1. Introducir al asistente a la publicación «desde cero», multiformato y 
   automatizada con _software_ libre o de código abierto.

### Particulares

1. Visualizar los problemas metodológicos entre ecosistemas editoriales
   cerrados y la necesidad multiformato.
2. Aprender metodologías ramificadas de producción editorial.
3. Explicar la pertinencia de un árbol de directorios consistente.
4. Usar formatos abiertos para explicitar su versatilidad.
5. Practicar uso de la terminal como método de empoderamiento tecnológico.
6. Explotar tecnologías abiertas para la ejecución y mantenimiento de 
   proyectos.
7. Experimentar en el retorno a ecosistemas editoriales cerrados u otros 
   caminos para la producción de impresos.

## Propuesta de cronograma

Todas las sesiones serían entresemana de nueve a once de la mañana para
las primeras cinco sesiones y de nueve a doce para las últimas. Por la
cantidad de actividades a realizar, se propone el siguiente horario:

01. Sesión 1: 22 de octubre (lunes).
02. Sesión 2: 23 de octubre (martes).
03. Sesión 3: 24 de octubre (miércoles).
04. Sesión 4: 25 de octubre (jueves).
05. Sesión 5: 29 de octubre (lunes).
06. Sesión 6: 30 de octubre (martes).
07. Sesión 7: 5 de noviembre (lunes).
08. Sesión 8: 6 de noviembre (martes).
09. Sesión 9: 7 de noviembre (miércoles).
10. Sesión 10: 8 de noviembre (jueves).

## Requisitos previos

Para una gestación fluida del taller, es recomendable que los asistentes
cuenten con lo siguiente:

* Un equipo de cómputo, se recomienda sistemas +++UNIX+++ (macOS o Linux).
* Conocimientos generales de procesos editoriales tradicionales.
* Conocimientos sobre cómo instalar programas.
* Idea básica de estructuras +++HTML+++.

## Necesidades espaciales

El taller supone que se realizará en un espacio que tiene lo siguiente:

* Conexión a internet.
* Proyector o pintarrón.

</section>
<section epub:type="chapter" role="doc-chapter">

# _Syllabus_

## Sesión 1: Más allá de Adobe

Adobe ha mutado de una _suite_ de diseño a una metodología única de
producción editorial. Al parecer Adobe lo tiene todo, su uso es demandado
por el sector editorial y su aprendizaje brota desde las universidades
hasta seminarios y talleres profesionales. Pero tiene costos ocultos.
El más grave es la reducción de una profesión y un método al aprendizaje
basado en el consumo de _software_. ¿Qué pasará si un día Adobe tiene la 
misma fortuna que QuarkXPress o PageMaker?

Esta primera sesión comenzará con una confrontación cara a cara con el 
formateo del texto creado en ecosistemas cerrados.

### Temas

1. Exportación a +++HTML+++ desde InDesign.
2. Análisis de estructuras textuales y hojas de estilo.
3. Demostración de la necesidad de limpieza del formato.
4. Introducción a RegEx como vía para la limpieza del formato.
5. Instalación de _software_ necesario para el taller.

## Sesión 2: Bienvenido a la edición «desde cero»

En la periferia del ecosistema de Adobe nos encontramos con una pluralidad
de programas y métodos. No siempre son compatibles entre sí. En la mayoría 
de los casos esta diversidad deviene en frustración. 

En esta sesión se iniciará el traslado del aprendizaje basado en programas 
a uno fundado en métodos, empezando por la manera adecuada de limpiar el
formato de un texto.

### Temas

1. Aprendizaje de los elementos más comunes de RegEx para la limpieza de 
   un texto.
2. Uso de RegEx en el texto de muestra.

## Sesión 3: ¡Esto es Pecas! > Herramientas editoriales libres

Gracias a estas herramientas hacer un +++EPUB+++ nunca había sido tan 
fácil como lo es ahora. En un entorno de código y según el método 
ramificado de edición es como surgieron estas increíbles herramientas 
editoriales. Una serie de _scripts_ automatizados pueden llegar 
a hacer la diferencia si se utilizan de manera correcta.

En este primer acercamiento nos interesa trabajar algunos párametros 
básicos que son parte de nuestro proyecto editorial. Así podremos 
formar nuestro primer +++EPUB+++.

### Temas 

1. ¿Qué es Pecas?
2. Revisión de párametros (repaso sencillo).
3. ¿Qué es el archivo +++YAML+++ y cómo es su sintaxis?
4. Uso de bash.

## Sesión 4: De la planicie y los tornados al bosque y las ramas

¿Tiene que haber una mejor manera, cierto? El formateo de texto es el
cuello de botella de la edición. No hay mejor manera de visualizarlo que
haber sufrido en la limpieza del texto. Esto abre la puerta para teorizar 
en dos orientaciones metodológicas para producción estandarizada de 
publicaciones: la edición cíclica y la edición ramificada.

En esta sesión se teorizará sobre las ventajas y límites de cada 
orientación metodológica para la edición.

### Temas

1. Bases generales de la edición digital.
2. Tipos de edición digital.
3. Tipos de edición estandarizada.
4. Edición tradicional.
5. Edición cíclica.
6. Edición ramificada.
7. Ejemplos de edición ramificada.

## Sesión 5: Bienvenidos al mundo de Markdown

Olvidémonos de las etiquetas +++HTML+++ y conozcamos Markdown, un 
lenguaje de marcado ligero que nos facilita la escritura en texto 
plano. Esta herramienta aparte de tener una sintaxis sencilla, nos 
brinda control en la estructura y facilita la atribución de los 
estilos. Ya no es necesario pasar del Word al código; con este 
lenguaje ¡lo tenemos todo!

En esta sesión aprenderemos a crear nuestro propio árbol de 
directorios, conoceremos su eficacia y el por qué son tan importantes. 
Veremos con esto la creación de nuestro primer documento `.md` y la 
sintaxis que requiere.

### Temas

1. ¿Qué es un árbol de directorios?
2. Moviéndonos entre carpetas.
3. Rutas relativas.
4. ¿Cuáles son los enfoques +++WYSIWYG+++ y +++WYSIWYM+++?
5. Introducción a Markdown.
6. Sintaxis de Markdown.

## Sesión 6: Nuestro primer proyecto con Markdown

Ya vimos que Markdown es un diamante en bruto, ahora solo nos falta 
ponerlo a prueba y construir nuestro propio contenido con sintaxis de
Markdown.

En esta etapa de seguimiento haremos nuestra primera publicación con 
Markdown, navegaremos entre carpetas y conoceremos el árbol de 
directorios a través de las rutas relativas.

### Temas

1. ¿Cómo trabajar con Markdown desde editores de texto?
2. Uso del formato +++MD+++.
3. Uso de clases e identificadores con Markdown.
4. Uso de clases por defecto de Pecas Markdown.

## Sesión 7: ¿Otra vez Pecas?

Es cierto que ya vimos estas herramientas editoriales, pero, ¿qué tan 
claro ha quedado su funcionamiento? Es importante conocer cuáles son 
los parámetros obligatorios u opcionales para su buen desempeño. Con
esto veremos cada una de las funcionalidades y lo que podemos hacer 
con ellas.

En este apartado conoceremos a fondo cómo utilizar Pecas y cómo 
podemos combinar los parámetros según las necesidades de cada proyecto 
editorial.

### Temas

1. Comandos de bash.
2. Herramientas para trabajar con nuestros archivos madre: `pc-add`,
	`pc-analytics`, `pc-images` y `pc-pandog`.
3. Herramientas para +++EPUB+++: `pc-automata`, `pc-creator`, 
   `pc-divider`, `pc-notes`, `pc-cites`, `pc-index`, `pc-recreator` y
   `pc-changer`.
4. Verificación de +++EPUB+++: `epubcheck` 3 y 4.
5. Estado de Pecas y sus dependencias: `pc-doctor`.

## Sesión 8: Mi primer +++EPUB+++

Un +++EPUB+++ es una serie de archivos +++XHTML+++ que son comprimidos 
en un archivo +++ZIP+++ para su portabilidad y su fácil lectura. 
Nosotros ya nos enfocamos en hacer nuestro contenido y lo que conlleva 
crear una publicación. Solo nos falta conocer el proceso para generar 
nuestro +++EPUB+++ en versiones 3.0.1, 3.0.0 y el +++MOBI+++ para 
Kindle.

En esta sesión veremos cómo comprimir un +++EPUB+++ desde la terminal 
utilizando Pecas.

### Temas

1. ¿Cómo comprimir un +++EPUB+++?
2. ¿Qué parámetros vamos a emplear para nuestra publicación?
3. ¿De qué nos sirve la analítica?
4. Verificación de +++EPUB+++ con EpubCheck 3 y 4.

## Sesión 9: Viaje a la exósfera

En el desarrollo de un proyecto tenemos necesidad de respaldar la 
información. Cuando concluimos también precisamos archivarlo. ¿Cuántas 
veces se ha vuelto un dolor de cabeza el uso de discos duros o de nubes? 
¿Cuántas veces no nos hemos confundido y hemos trabajado doble o, peor aún, 
se ha perdido información?

En este sesión trataremos otro método para poder tener un control sobre
nuestro proyecto, en el que los discos duros y las nubes se vuelven
innecesarios.

### Temas

1. ¿Qué es `git`? ¿Qué es un proyecto como repositorio remoto?
2. Creación de cuentas para el manejo de repositorios remotos.
3. Creación de llaves de acceso.
4. Creación de equipos de trabajo.
5. Creación de repositorios.
6. Clonación local de repositorios.
7. Dinámica de `pull` y `push`.
8. Visualización de estados e historial de repositorios.

## Sesión 10: Regreso a la tierra

¿Quieres un impreso? La edición ramificada vista aquí no necesariamente
implica una apertura en los ecosistemas editoriales. Es posible regresar
a ecosistemas cerrados para crear un +++PDF+++ para impresión, aunque
también desde hace más de treinta años existe otra vía…

### Temas

1. Conversión de +++MD+++ a +++XML+++.
2. Importación de +++XML+++ a InDesign.
3. Asociación de etiquetas +++XML+++ con estilos de párrafo o de 
   caracteres.
4. ¿Qué es TeX? ¿Qué es LaTeX?
5. Conversión de +++MD+++ a TeX.
6. Aplicación de plantillas de LaTeX.
7. Creación de +++PDF+++ con LaTeX.

</section>
<section epub:type="chapter" role="doc-chapter">

# Costos

Las veinticinco horas de taller más una memoria en video tienen un costo de
$25,000.00 pesos más +++IVA+++.

Se requiere un anticipo del 50% y lo restante para más tardar la última
sesión del taller.

El taller no tiene cupo limitado, aunque para una mejor experiencia se
recomienda que no sea mayor a diez asistentes.

El precio ya incluye toda la infraestructura y materiales necesarios para
el taller y la producción de la memoria.

La memoria será propiedad de los asistentes, los talleristas no pondrán
restricciones sobre su uso.

</section>
